#!/usr/bin/env python
# coding: utf-8

import MySQLdb

def insert(dbname, username, passwd, cols, vals):
    conn = MySQLdb.connect(host="localhost", db=dbname, user=username, passwd=passwd, charset="utf8mb4")
    cursor = conn.cursor()

    sql="INSERT INTO news (" + cols[0]
    for i in range(1, len(cols)):
        sql += "," + cols[i]

    sql += ") values (" + vals[0]
    for i in range(1, len(vals)):
        if type(vals[i]) == type("string"): sql += ',"' + vals[i] + '"'
        else: sql += "," + str(vals[i])
    sql += ')'

    try:
        cursor.execute(sql)
        cursor.fetchall()
    except:
        print("Error")
    
    cursor.close()
    conn.commit()
    conn.close()

def news_data_collection(dbname, username, passwd, cols, tbl, condition=""):
    conn = MySQLdb.connect(host="localhost", db=dbname, user=username, passwd=passwd, charset="utf8mb4")
    cursor = conn.cursor()
    sql = "select " + cols + " from " + tbl
    if len(condition) > 0: sql += " where " + condition
    cursor.execute(sql)
    return cursor
