#-*- coding: utf-8 -*-
#!/usr/bin/python

import pickle
import nltk
import numpy as np

from gensim import corpora, models
from gensim.models.doc2vec import TaggedDocument
from gensim.models.doc2vec import Doc2Vec
from gensim.models import word2vec

lemmatizer = nltk.stem.WordNetLemmatizer()

nepock = 20
tnum = 300
dsize = 300
alpha_delta = (0.025 - 0.0001) / 19

def invoke(p="", nb=1, na=0.1, tnum=100):
    texts = [
        lem("This is a test"),
        lem("hello world")
    ]
    make_models(p, texts, nb, na, tnum)

def lem(texts):
    return [lemmatizer.lemmatize(w) for text in texts for w in text.lower().split()]

def load_models(p, na, nb, tnum):
    dct = corpora.Dictionary.load(p + "_" + str(na) + "_" + str(nb) + '.dict')
    tfidf = models.TfidfModel.load(p + "_" + str(na) + "_" + str(nb) + '_tfidf.model')
    lsi = models.LsiModel.load(p + '_lsi_' + str(na) + str(nb) + "_" + str(tnum) + '_topics.model')
    lda = models.LdaModel.load(p + '_lda_' + str(na) + str(nb) + "_" + str(tnum) + '_topics.model')
    d2v = models.Doc2Vec.load(p + "_" + str(na) + "_" + str(nb) + "_" + str(tnum) + '_doc2vec.model')
    return [dct, tfidf, lsi, lda, d2v]

def make_models(p, token_list, nb, na, tnum):
    print ("make a dictionary")
    dct = corpora.Dictionary(token_list)
    if nb > 0 and na > 0:
        dct.filter_extremes(no_below=nb, no_above=na)
    dct.save(p + "_" + str(na) + "_" + str(nb) + '.dict')

    corp = [dct.doc2bow(tlist) for tlist in token_list]

    fname = p + "_" + str(na) + "_" + str(nb) + '_corp.pkl'
    pickle.dump(corp, open(fname, 'wb'), protocol=2)

    print ("tf-idf")
    tfidf = models.TfidfModel(corp)
    tfidf.save(p + "_" + str(na) + "_" + str(nb) + '_tfidf.model')

    corp_tfidf = tfidf[corp]

    print ("lsi")
    lsi = models.LsiModel(corpus=corp, id2word=dct, num_topics=tnum)
    lsi.save(p + '_lsi_' + str(na) + str(nb) + "_" + str(tnum) + '_topics.model')

    lsi = models.LsiModel(corpus=corp_tfidf, id2word=dct, num_topics=tnum)
    lsi.save(p + '_tfidf_lsi_' + str(na) + str(nb) + "_" + str(tnum)+'_topics.model')

    print ("lda")
    lda = models.LdaModel(corpus=corp, id2word=dct, num_topics=tnum)
    lda.save(p + '_lda_' + str(na) + str(nb) + "_" + str(tnum) + '_topics.model')

    lda = models.LdaModel(corpus=corp_tfidf, id2word=dct, num_topics=tnum)
    lda.save(p + '_tfidf_lda_' + str(na) + str(nb) + "_" + str(tnum) + '_topics.model')

    print("w2v")
    model = word2vec.Word2Vec(token_list, sg=1, size=100, min_count=1, window=10, hs=1, negative=0)
    model.save(p + "_" + str(na) + "_" + str(nb) + "_" + str(tnum) + "_word2vec.model")

    print ("d2v")
    d2v_sentences = []
    for i in range(len(token_list)):
        d2v_sentences.append(TaggedDocument(words=token_list[i], tags=['d' + str(i)]))

    model = models.Doc2Vec(d2v_sentences, dm=0, size=300, window=15, alpha=.025, min_alpha=.025, min_count=1, sample=1e-6)
    for epoch in range(nepock):
        model.train(d2v_sentences, total_examples=len(d2v_sentences), epochs=model.epochs)
        model.alpha -= alpha_delta
        model.min_alpha = model.alpha

    model.save(p + "_" + str(na) + "_" + str(nb) + "_" + str(tnum) + '_doc2vec.model')

if __name__ == "__main__":
    invoke()
