#!/usr/bin/env python
# coding: utf-8

from bs4 import BeautifulSoup
import requests
import re
 
def get_html_source(wikipedia_url):
    res = requests.get(wikipedia_url)
    return res.text

def infobox_with_scraping(txt):
    soup = BeautifulSoup(txt, "html.parser")
    infobox = soup.find(class_=re.compile('infobox vevent'))
 
if __name__ == '__main__':
	url = 'https://en.wikipedia.org/wiki/Attack_on_Pearl_Harbor'
    html_txt = get_html_source(url)
    infobox_with_scraping(html_txt)
