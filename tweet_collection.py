#!/usr/bin/env python
# coding: utf-8

import tweepy
from datetime import timedelta
import pickle

consumer_key =""#use your key
consumer_secret =""#use your key
access_token=""#use your key
access_token_secret =""#use your key
 
auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)

def usertimeline(account, count=3):
    tweets = api.user_timeline(screen_name=account, count=count, tweet_mode = 'extended')

    for tweet in tweets:
        tweet_id = tweet.id
        last_tweet_ids[account] = tweet.id
        retweet = tweet.retweet_count
        favo = tweet.favorite_count
        date = tweet.created_at
        created_at = (tweet.created_at + timedelta(hours=9)).strftime('%Y/%m/%d %H:%M:%S')
        full_text =  tweet.full_text
        pickle.dump([tweet_id, full_text], open("tweets.py", 'wb'), protocol=2)


Accounts = ["Yomiuri_Online"]
for ac in Accounts:
    usertimeline(ac, 10)
