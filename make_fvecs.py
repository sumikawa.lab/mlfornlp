#!/usr/bin/env python
# coding: utf-8

from gensim import corpora, models

def invoke():
	token_list = [
	["this", "is", "a", "test"], 
	["no", "test"], 
	]
    models = load_models()
    fvecs = make_fvecs(token_list, models)

def make_fvecs(token_list, models, tnum=300):
    dct, tfidf, lsi = models[0], models[1], models[2]
    tfidf_fvecs = []
    lsi_fvecs = []
    for tokens in token_list:
        tfidf_v = [m2v(tfidf[dct.doc2bow(tokens)], len(dct))]
        tfidf_fvecs.append(tfidf_v)

        lsi_v = [m2v(lsi[dct.doc2bow(tokens)], tnum)]
        lsi_fvecs.append(lsi_v)
    return [tfidf_fvecs, lsi_fvecs]

def m2v(v, l):
    vmap = dict([(t[0], t[1]) for t in v])
    vec = []
    for i in range(l):
        if i in vmap: vec.append(float(vmap[i]))
        else: vec.append(0.0)
    return vec

def load_models(na=0.2, nb=3, tnum=300):
    dct = corpora.Dictionary.load("retweet_tokens" + str(nb) + "_" + str(na) + ".dct")
    tfidf = models.TfidfModel.load("tfidf_" + str(nb) + "_" + str(na) + '.model')
    lsi = models.LsiModel.load('lsi_' + str(nb) + "_" + str(na) + "_" + str(tnum) + '_topics.model') 
    return [dct, tfidf, lsi]

def model_creation(token_list, na=0.2, nb=3):
    dct = corpora.Dictionary(token_list)
    if nb > 0 and na > 0:
        dct.filter_extremes(no_below=nb, no_above=na)
    dct.save("tokens" + str(nb) + "_" + str(na) + ".dct")

    corpus = [dct.doc2bow(tokens) for tokens in token_list]
    tfidf = models.TfidfModel(corpus)
    tfidf.save("tfidf_" + str(nb) + "_" + str(na) + '.model')

    tnum = 300
    lsi = models.LsiModel(corpus=corpus, id2word=dct, num_topics=tnum)
    lsi.save('lsi_' + str(nb) + "_" + str(na) + "_" + str(tnum) + '_topics.model')  

if __name__ == "__main__":
    invoke()
