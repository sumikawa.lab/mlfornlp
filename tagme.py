#!/usr/bin/env python
# coding: utf-8

import json
import requests

def applyingTagme(access_token, txt):
    """
    access_token: use your token access key provided by TAGME.
    txt: a text for annotation
    """
    url = "https://tagme.d4science.org/tagme/tag?lang=en&gcube-token=" + access_token + "&text=" + txt
    res = requests.get(url=url).json()
    elist = res["annotations"]
    etitles = [e["title"] for e in elist]
    return etitles

txt = "obama visited uk"
actoken = ""
applyingTagme(actoken, txt)
